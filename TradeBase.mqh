//+------------------------------------------------------------------+
//|                                                     Position.mqh |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property version   "1.00"
#property strict

#define SORT_CLOSE_TIME 8

#include <Arrays\ArrayObj.mqh>
#include "SymbolInfo.mqh"
enum TRADE_TYPE
{
   TRADE_BASE,
   TRADE_POSITION,
   TRADE_ORDER,
   TRADE_DEAL
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CTradeBase : public CObject
{
protected:
   int               m_ticket;
public:
                     CTradeBase(int ticket):m_ticket(ticket){}
   virtual bool      OrderSelect()     { return _Select(); }
   virtual string    OrderSymbol()     { if(_Select())return ::OrderSymbol();    else return "";}
   virtual string    OrderComment()    { if(_Select())return ::OrderComment();   else return "";}
   
   virtual double    OrderClosePrice() { if(_Select())return ::OrderClosePrice();else return 0.0;}             
   virtual double    OrderOpenPrice()  { if(_Select())return ::OrderOpenPrice(); else return 0.0;}                 
   virtual double    OrderLots()       { if(_Select())return ::OrderLots();      else return 0.0;}
   virtual double    OrderCommission() { if(_Select())return ::OrderCommission();else return 0.0;}
   virtual double    OrderProfit()     { if(_Select())return ::OrderProfit();    else return 0.0;}
   virtual double    OrderStopLoss()   { if(_Select())return ::OrderStopLoss();  else return 0.0;}
   virtual double    OrderTakeProfit() { if(_Select())return ::OrderTakeProfit();else return 0.0;}
   virtual double    OrderSwap()       { if(_Select())return ::OrderSwap();      else return 0.0;}

   virtual int       OrderTicket()     { return m_ticket; }
   virtual int       OrderType()       { if(_Select())return ::OrderType();       else return -1;}
   virtual int       OrderMagicNumber(){ if(_Select())return ::OrderMagicNumber();else return -1;}
   
   virtual datetime  OrderExpiration() { if(_Select())return ::OrderExpiration();else return 0;}
   virtual datetime  OrderOpenTime()   { if(_Select())return ::OrderOpenTime();  else return 0;}
   virtual datetime  OrderCloseTime()  { if(_Select())return ::OrderCloseTime(); else return 0;}
   virtual double    TickValue();
   virtual double    MoneyAtRisk();                   
   virtual bool      Modify(double,double,double){ return true; }
   virtual bool      Refresh() { return true;}
   virtual int       Compare(const CObject *node,const int mode=0)const override;
   virtual TRADE_TYPE Type() { return TRADE_BASE; }
protected:
   bool              _Select()         { return ::OrderSelect(m_ticket,SELECT_BY_TICKET); }
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CPosition : public CTradeBase
{
public:
                     CPosition(int ticket):CTradeBase(ticket){}
   virtual TRADE_TYPE Type() { return TRADE_POSITION; }
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CDeal : public CTradeBase
{
public:
                     CDeal(int ticket):CTradeBase(ticket){}
   virtual TRADE_TYPE Type() { return TRADE_DEAL; }
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class COrder : public CTradeBase
{
public:
                     COrder(int ticket):CTradeBase(ticket){}
   virtual TRADE_TYPE Type() { return TRADE_ORDER; }
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CTradeVector : public CArrayObj
{
public:
   CTradeBase  *operator[](const int index)const{return (CTradeBase*)At(index);}
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double CTradeBase::TickValue(void)
{
   if(_Select())
      return ::SymbolInfoDouble(OrderSymbol(),SYMBOL_TRADE_TICK_VALUE);
   return 0;
}

double CTradeBase::MoneyAtRisk(void)
{
   if(this.OrderStopLoss() == 0.0)
      return 0;
   CSymbolInfo sym;
   sym.Name(this.OrderSymbol());
   double money_per_point = TickValue();
   double points = 0.0;
   int t = this.OrderType();
   double lots = this.OrderLots();
   if(t==OP_BUY||t==OP_BUYLIMIT||t==OP_BUYSTOP)
   {
      points = this.OrderOpenPrice() - this.OrderStopLoss();
      points = ::NormalizeDouble(points,sym.Digits());
   }
   else
   if(t==OP_SELL||t==OP_SELLLIMIT||t==OP_SELLSTOP)
   {
      points = this.OrderStopLoss() - this.OrderOpenPrice();
      points = ::NormalizeDouble(points,sym.Digits());
   }
   if(points > 0)
   {
      double p = sym.Point();
      points/=p;
      return money_per_point*points*lots;
   }
   return 0.0;
}