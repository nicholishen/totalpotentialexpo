//+------------------------------------------------------------------+
//|                                                 Mql4TradeOps.mqh |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property version   "1.00"
#property strict
#include <stdlib.mqh>
#include <Arrays\ArrayObj.mqh>
#include <Arrays\ArrayInt.mqh>
#include "TradeBase.mqh"

#define ALL_ORDER_TYPES 1000001
/*
Common functionality:
   1. Trailing stop
   2. Move stop to break-even
   3. Track profit/loss
   4. Place trades with MM
   5. Close Trades
   6. Limit trading hours
   7. Limit # of trades 
   8. Send Notifications
   9. Comment TradeServer in upper right corner
   10. FixPips
   11. CloseAll

*/

enum WORKING_MODE
{
   WORKING_POSITIONS,
   WORKING_ORDERS,
   WORKING_DEALS,
   WORKING_POS_ORD,
   WORKING_POS_DEALS
};

enum MONEY_MANAGEMENT
{
   MONEY_FIXED,
   MONEY_RISK
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CMql4TradeOps : public CObject
{
protected:
   string            m_symbol;
   string            m_ea_name;
   int               m_magic;
   ENUM_TIMEFRAMES   m_period;
   int               m_slippage;
   //double            m_point;
   //int               m_digits;
   //double            m_fixPoint;
   //int               m_fixDigits;
   
   int               m_hour_from;
   int               m_hour_to;
   
   CTradeVector      m_trades;
   
   MONEY_MANAGEMENT  m_money;
   double            m_fixed_lots;
   color             m_arrow_color;
   int               m_max_orders_bar;
   ENUM_TIMEFRAMES   m_order_limit_period;
   
   bool              m_showTradeServer;
   bool              m_fixPips;
   bool              m_filterBySymbol;
   bool              m_notifyOnFail;
   
   WORKING_MODE      m_orderMode;
public:
                     CMql4TradeOps();
                    ~CMql4TradeOps();
   CTradeBase       *operator[](const int index)const{ return m_trades[index]; }
   void              Init( const int         magic    = NULL,
                           string            ea_name  = "Easy Lib",
                           MONEY_MANAGEMENT  money_mode = MONEY_FIXED,
                           ENUM_TIMEFRAMES   period   = PERIOD_CURRENT,
                           const int         slippage = 0,
                           const bool        fixPips  = false);
                           
   color             ArrowColor()                  { return m_arrow_color;                }
   void              ArrowColor(color col)         { m_arrow_color = col;                 }
   bool              FilterBySymbol()        const { return m_filterBySymbol;             }
   void              FilterBySymbol(bool filter)   { m_filterBySymbol = filter;           }
   bool              FixPips()               const { return m_fixPips ;                   }
   void              FixPips(bool fixem)           { m_fixPips = fixem;                   }
   double            LotsFixed()             const { return m_fixed_lots;                 }
   void              LotsFixed(double lots)        { m_fixed_lots = lots;                 } 
   void              NotifyOnOrderFail(bool notify){ m_notifyOnFail = notify;             }
   void              RestrictTradeHours(int from,int to);
   void              SymbolLocked(string symbol)   { m_symbol = symbol;                   }
   bool              SymbolLocked()                { return m_symbol != NULL;             }
   //bool              Select(const int i);
   int               TotalPositions(int type = ALL_ORDER_TYPES,string symbol = NULL);      
   int               TotalOrders(int type = ALL_ORDER_TYPES,string symbol = NULL);                   
   int               TotalPositionsAndOrders(int type = ALL_ORDER_TYPES,string symbol = NULL);            
   int               TotalPositionsAndDeals(int type = ALL_ORDER_TYPES,string symbol = NULL);       
   int               TotalDeals(int type = ALL_ORDER_TYPES,string symbol = NULL);                  
   double            TotalProfitHistory(int type = ALL_ORDER_TYPES,string symbol = NULL);
   double            TotalProfitFloating(int type = ALL_ORDER_TYPES,string symbol = NULL);   
   
   void              MaxMarketOrdersPerBar(ENUM_TIMEFRAMES period = PERIOD_CURRENT,int limit =1);
   
   bool              CloseAll(string symbol = NULL);
   bool              CloseAllOrders(string symbol = NULL);
   bool              CloseAllPositions(string symbol = NULL);
   bool              CloseOrder(int ticket);
   
   bool              BuyMarket(double sl = 0.0,double tp = 0.0,double lots=0.000000,string symbol=NULL);
   bool              BuyMarket(int sl,int tp,double lots=0.000000,string symbol=NULL);
   bool              BuyLimit(double price,double sl=0.000000,double tp=0.000000,double lots=0.000000,string symbol=NULL);
   bool              BuyStop(double price,double sl=0.000000,double tp=0.000000,double lots=0.000000,string symbol=NULL);
   
   bool              SellMarket(int sl,int tp,double lots=0.000000,string symbol=NULL);
   bool              SellMarket(double lots=0.000000,double sl = 0.0,double tp = 0.0,string symbol=NULL);
   bool              SellLimit(double price,double sl=0.000000,double tp=0.000000,double lots=0.000000,string symbol=NULL);
   bool              SellStop(double price,double sl=0.000000,double tp=0.000000,double lots=0.000000,string symbol=NULL);
   
protected:
   
   bool              _PendingOrder(int type,double price,double sl=0.000000,double tp=0.000000,double lots=0.000000,string symbol=NULL);
   int               _GetPositions(int order_type = ALL_ORDER_TYPES,string symbol = NULL);
   int               _GetOrders(int order_type = ALL_ORDER_TYPES,string symbol = NULL);
   int               _GetDeals(int order_type = ALL_ORDER_TYPES,string symbol = NULL);
   int               _Get_Pos_Orders(int order_type = ALL_ORDER_TYPES,string symbol = NULL);
   int               _Get_Pos_Deals(int order_type = ALL_ORDER_TYPES,string symbol = NULL);
   double            _Bid(string symbol=NULL);   
   double            _Ask(string symbol=NULL);  
/************************ Lots needs work... the stoploss param makes no sense for risk ****************/ 
   double            _Lots(string symbol=NULL,double stop_loss=0.0);
   bool              _CanTrade(int order_type,string symbol = NULL);
   bool              _IsResrictedTime() const;
   
   double            _Point(string symbol = NULL);
   int               _Digits(string symbol = NULL);
   double            _AdjPoint(string symbol = NULL);
   int               _AdjDigits(string symbol = NULL);
   
   bool              _FixSymbol(string &symbol)const;
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CMql4TradeOps::CMql4TradeOps():m_symbol(NULL),m_notifyOnFail(true)
{
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CMql4TradeOps::~CMql4TradeOps()
{
}
//+------------------------------------------------------------------+
void CMql4TradeOps::Init(  const int         magic    = NULL,
                           string            ea_name  = "Easy Lib",
                           MONEY_MANAGEMENT  money_mode = MONEY_FIXED,
                           ENUM_TIMEFRAMES   period   = PERIOD_CURRENT,
                           const int         slippage = 0,
                           const bool        fixPips  = false)
{
   //m_symbol    = symbol == NULL           ? ::Symbol() : symbol;
   //m_fixed_lots= 0;
   m_magic     = magic  == NULL ? 0 : magic;
   m_period    = period == PERIOD_CURRENT ? (ENUM_TIMEFRAMES)::Period() : period;
   m_slippage  = slippage;
   m_fixPips   = fixPips;
   m_ea_name   = ea_name;
   m_arrow_color= clrNONE;
}
//+------------------------------------------------------------------+
//|   Trade Limiting Methods                                         |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// checks to see if a trade can be placed. Checks all trading rules and returns true if a trade can be placed. 
bool CMql4TradeOps::_CanTrade(int order_type,string symbol=NULL)
{
   bool res = true;
   _FixSymbol(symbol);
   if(m_max_orders_bar > 0)
   {
      int cnt = 0;
      int total = _Get_Pos_Deals(order_type,symbol);
      for(int i=0;i<total;i++)
      {
         if(this[i].OrderSelect())
         {
            if(::OrderCloseTime() >= ::iTime(symbol,m_order_limit_period,0) || ::OrderOpenTime() >= ::iTime(symbol,m_order_limit_period,0))
               cnt++;
         }     
      }
      if(cnt >= m_max_orders_bar)
         res = false;
   }
   if(_IsResrictedTime())
      res = false;
   return res;
}
//+------------------------------------------------------------------+
void CMql4TradeOps::MaxMarketOrdersPerBar(ENUM_TIMEFRAMES period=PERIOD_CURRENT,int limit=1)
{
   period = period == PERIOD_CURRENT ? (ENUM_TIMEFRAMES)::Period() : period;
   m_max_orders_bar = limit;
   m_order_limit_period = period;
}
//+------------------------------------------------------------------+
void CMql4TradeOps::RestrictTradeHours(int from,int to)
{
   m_hour_from = from;
   m_hour_to   = to;
}
//+------------------------------------------------------------------+
bool CMql4TradeOps::_IsResrictedTime() const
{
   if(m_hour_from < 0 || m_hour_to < 0 || m_hour_from > 23 || m_hour_to > 23)
      return false; // ok to trade any time > or error
   MqlDateTime time;
   ::TimeToStruct(::TimeCurrent(),time);
   if(m_hour_from <= m_hour_to)
   {
      if(m_hour_from <= time.hour && time.hour <= m_hour_to)
         return true;
   }
   else if( !(m_hour_to < time.hour && time.hour < m_hour_from))
      return true;
   return false;
}
//+------------------------------------------------------------------+
//|   OPEN orders / BUY                                              |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
bool CMql4TradeOps::_PendingOrder(int type,double price,double sl=0.000000,double tp=0.000000,double lots=0.000000,string symbol=NULL)
{
    _FixSymbol(symbol);
   if(!_CanTrade(type,symbol))
      return false;
   lots   = lots   == 0.0  ? _Lots(symbol,sl) : lots;
   string comm = m_ea_name+"_"+::IntegerToString(m_magic);
   bool res =  ::OrderSend(symbol,type, lots,price,m_slippage,sl,tp,comm,m_magic);
   if(!res)
   {
      ::Print(__FUNCTION__," OrderSendError :",ErrorDescription(::GetLastError()));
      if(m_notifyOnFail)
         ::SendNotification(symbol+" BuyLimit Order failed: "+ErrorDescription(::GetLastError()));
   }
   return res; 
}

bool CMql4TradeOps::BuyMarket(double sl = 0.0,double tp = 0.0,double lots=0.000000,string symbol=NULL)
{
   _FixSymbol(symbol);
   if(!_CanTrade(OP_BUY,symbol))
      return false;
   lots   = lots   == 0.0  ? _Lots(symbol,sl) : lots;
   string comm = m_ea_name+"_"+::IntegerToString(m_magic);
   bool res = ::OrderSend(symbol,OP_BUY,lots,_Ask(symbol),m_slippage,sl,tp,comm,m_magic);
   if(!res)
   {
      ::Print(__FUNCTION__," OrderSendError :",ErrorDescription(GetLastError()));
      if(m_notifyOnFail)
         ::SendNotification(symbol+" Buy Order failed: "+ErrorDescription(::GetLastError()));
   }
   return res; 
}
//+------------------------------------------------------------------+
bool CMql4TradeOps::BuyMarket(int sl,int tp,double lots=0.000000,string symbol=NULL)
{
   _FixSymbol(symbol);
   double stop = _Ask(symbol) - sl*_AdjPoint(symbol);
   double take = _Ask(symbol) + tp*_AdjPoint(symbol);
   return BuyMarket(stop,take,lots,symbol);
}
//+------------------------------------------------------------------+
bool CMql4TradeOps::BuyLimit(double price,double sl=0.000000,double tp=0.000000,double lots=0.000000,string symbol=NULL)
{
   return(_PendingOrder(OP_BUYLIMIT,price,sl,tp,lots,symbol));
}
/*****************************************************************************

            TODO finish implementing the new method for pending orders

*******************************************************************************/
//+------------------------------------------------------------------+
bool CMql4TradeOps::BuyStop(double price,double sl=0.000000,double tp=0.000000,double lots=0.000000,string symbol=NULL)
{
   _FixSymbol(symbol);
   if(!_CanTrade(OP_BUYSTOP,symbol))
      return false;
   lots   = lots   == 0.0  ? _Lots(symbol,sl) : lots;
   string comm = m_ea_name+"_"+::IntegerToString(m_magic);
   bool res =  ::OrderSend(symbol,OP_BUYSTOP,lots,price,m_slippage,sl,tp,comm,m_magic);
   if(!res)
   {
      ::Print(__FUNCTION__," OrderSendError :",ErrorDescription(::GetLastError()));
      if(m_notifyOnFail)
         ::SendNotification(symbol+" BuyStop Order failed: "+ErrorDescription(::GetLastError()));
   }
   return res; 
}

//+------------------------------------------------------------------+
bool CMql4TradeOps::SellMarket(double sl = 0.0,double tp = 0.0,double lots=0.000000,string symbol=NULL)
{
   _FixSymbol(symbol);
   if(!_CanTrade(OP_SELL,symbol))
      return false;
   lots   = lots   == 0.0  ? _Lots(symbol,sl) : lots;
   string comm = m_ea_name+"_"+::IntegerToString(m_magic);
   bool res = ::OrderSend(symbol,OP_SELL,lots,_Bid(symbol),m_slippage,sl,tp,comm,m_magic);
   if(!res)
   {
      ::Print(__FUNCTION__," OrderSendError :",ErrorDescription(GetLastError()));
      if(m_notifyOnFail)
         ::SendNotification(symbol+" Buy Order failed: "+ErrorDescription(::GetLastError()));
   }
   return res; 
}
//+------------------------------------------------------------------+
bool CMql4TradeOps::SellMarket(int sl,int tp,double lots=0.000000,string symbol=NULL)
{
   _FixSymbol(symbol);
   double stop = _Bid(symbol) + sl*_AdjPoint(symbol);
   double take = _Bid(symbol) - tp*_AdjPoint(symbol);
   return SellMarket(stop,take,lots,symbol);
}

bool CMql4TradeOps::SellLimit(double price,double sl=0.000000,double tp=0.000000,double lots=0.000000,string symbol=NULL)
{
   _FixSymbol(symbol);
   if(!_CanTrade(OP_SELLLIMIT,symbol))
      return false;
   lots   = lots   == 0.0  ? _Lots(symbol,sl) : lots;
   string comm = m_ea_name+"_"+::IntegerToString(m_magic);
   bool res =  ::OrderSend(symbol,OP_SELLLIMIT, lots,price,m_slippage,sl,tp,comm,m_magic);
   if(!res)
   {
      ::Print(__FUNCTION__," OrderSendError :",ErrorDescription(::GetLastError()));
      if(m_notifyOnFail)
         ::SendNotification(symbol+" BuyLimit Order failed: "+ErrorDescription(::GetLastError()));
   }
   return res; 
}

bool CMql4TradeOps::SellStop(double price,double sl=0.000000,double tp=0.000000,double lots=0.000000,string symbol=NULL)
{
   _FixSymbol(symbol);
   if(!_CanTrade(OP_SELLSTOP,symbol))
      return false;
   lots   = lots   == 0.0  ? _Lots(symbol,sl) : lots;
   string comm = m_ea_name+"_"+::IntegerToString(m_magic);
   bool res =  ::OrderSend(symbol,OP_SELLSTOP, lots,price,m_slippage,sl,tp,comm,m_magic);
   if(!res)
   {
      ::Print(__FUNCTION__," OrderSendError :",ErrorDescription(::GetLastError()));
      if(m_notifyOnFail)
         ::SendNotification(symbol+" BuyLimit Order failed: "+ErrorDescription(::GetLastError()));
   }
   return res; 
}



//+------------------------------------------------------------------+
//|   Closing Orders Methods                                         |
//+------------------------------------------------------------------+
bool CMql4TradeOps::CloseOrder(int ticket)
{
   CTradeBase order(ticket);
   if(!order.OrderSelect())
      return false;
   if(!(order.OrderType() == OP_BUY || order.OrderType() == OP_SELL))
   {
      return ::OrderDelete(ticket,m_arrow_color);
   }
   else 
   {
      double price = order.OrderType() == OP_BUY ? _Bid(order.OrderSymbol()) : _Ask(order.OrderSymbol());
      return ::OrderClose(ticket,order.OrderLots(),price,m_slippage,m_arrow_color);
   }
}
//+------------------------------------------------------------------+
bool CMql4TradeOps::CloseAll(string symbol = NULL)
{
   bool res = true;
   if(!CloseAllPositions(symbol))
      res = false;
   if(!CloseAllOrders(symbol))
      res = false;
   return res;
}
//+------------------------------------------------------------------+
bool CMql4TradeOps::CloseAllPositions(string symbol = NULL)
{
   bool res = true;
   int total = _GetPositions(ALL_ORDER_TYPES,symbol);
   for(int i=0;i<total;i++)
   {
      if(this[i].OrderSelect())
      {
         if(!CloseOrder(this[i].OrderTicket()))    
            res = false;
      }
   }
   return res;
}
//+------------------------------------------------------------------+
bool CMql4TradeOps::CloseAllOrders(string symbol = NULL)
{
   bool res = true;
   int total = _GetOrders(ALL_ORDER_TYPES,symbol);
   for(int i=0;i<total;i++)
   {
      if(this[i].OrderSelect())
      {
         if(!CloseOrder(this[i].OrderTicket()))    
            res = false;
      }
   }
   return res;
}
//+------------------------------------------------------------------+
//|   Total Methods                                                  |
//+------------------------------------------------------------------+
double CMql4TradeOps::TotalProfitFloating(int type = ALL_ORDER_TYPES,string symbol = NULL)
{
   double res = 0;
   int total = _GetPositions(type,symbol);
   for(int i=0;i<total;i++)
   {
      res+= this[i].OrderProfit();    
   }
   return res;
}
//+------------------------------------------------------------------+
double CMql4TradeOps::TotalProfitHistory(int type = ALL_ORDER_TYPES,string symbol = NULL)
{
   double res = 0;
   int total = _GetDeals(type,symbol);
   for(int i=0;i<total;i++)
   {
      res+= this[i].OrderProfit();    
   }
   return res;
}
//+------------------------------------------------------------------+
int  CMql4TradeOps::TotalPositions(int type = ALL_ORDER_TYPES,string symbol = NULL)      
{  
   return _GetPositions(type,symbol); 
}
//+------------------------------------------------------------------+
int  CMql4TradeOps::TotalOrders(int type = ALL_ORDER_TYPES,string symbol = NULL)                 
{ 
   return _GetOrders(type,symbol);   
}  
//+------------------------------------------------------------------+   
int  CMql4TradeOps::TotalPositionsAndOrders(int type = ALL_ORDER_TYPES,string symbol = NULL)     
{ 
   return _Get_Pos_Orders(type,symbol);  
} 
//+------------------------------------------------------------------+        
int  CMql4TradeOps::TotalPositionsAndDeals(int type = ALL_ORDER_TYPES,string symbol = NULL)     
{ 
   return _Get_Pos_Deals(type,symbol);
}   
//+------------------------------------------------------------------+   
int  CMql4TradeOps::TotalDeals(int type = ALL_ORDER_TYPES,string symbol = NULL)                  
{  
   return _GetDeals(type,symbol);
}  
//+------------------------------------------------------------------+
//|   Private Methods                                                |
//+------------------------------------------------------------------+
double CMql4TradeOps::_Bid(string symbol=NULL)
{
   _FixSymbol(symbol);
   return ::NormalizeDouble(SymbolInfoDouble(symbol,SYMBOL_BID),_Digits(symbol)); 
}
//+------------------------------------------------------------------+
double CMql4TradeOps::_Ask(string symbol=NULL)
{
   _FixSymbol(symbol);
   return ::NormalizeDouble(SymbolInfoDouble(symbol,SYMBOL_ASK),_Digits(symbol)); 
}
//+------------------------------------------------------------------+
int CMql4TradeOps::_GetDeals(int order_type = ALL_ORDER_TYPES,string symbol = NULL)
{
   bool isNull = _FixSymbol(symbol);
   m_trades.Clear();
   int total = ::OrdersHistoryTotal();
   for(int i=0;i<total;i++)
   {
      if(::OrderSelect(i,SELECT_BY_POS,MODE_HISTORY))
      {
         if((isNull || ::OrderSymbol() == symbol)) 
         {
            if((m_magic == 0 || ::OrderMagicNumber() == m_magic))
            {
               if(
                     (order_type==ALL_ORDER_TYPES && (::OrderType() == OP_BUY||::OrderType() == OP_SELL))
                     ||
                     (order_type == ::OrderType())
                  )
               {
                  m_trades.Add(new COrder(::OrderTicket()));
               }
            }
          }
      }
      else
         return -1;
   }
   m_orderMode = WORKING_DEALS;
   return m_trades.Total();
}
//+------------------------------------------------------------------+
int CMql4TradeOps::_GetPositions(int order_type = ALL_ORDER_TYPES,string symbol = NULL)
{
   bool isNull = _FixSymbol(symbol);
   m_trades.Clear();
   int total = ::OrdersTotal();
   for(int i=0;i<total;i++)
   {
      if(::OrderSelect(i,SELECT_BY_POS))
      {
         if((isNull || ::OrderSymbol() == symbol)) 
         {
            if((m_magic == 0 || ::OrderMagicNumber() == m_magic))
            {
               if(
                     (order_type==ALL_ORDER_TYPES && (::OrderType() == OP_BUY || ::OrderType() == OP_SELL))
                     ||
                     (order_type == ::OrderType())
                  )
               {
                  m_trades.Add(new COrder(::OrderTicket()));
               }
            }
          }
      }
      else
         return -1;
   }
   m_orderMode = WORKING_POSITIONS;
   return m_trades.Total();
}
//+------------------------------------------------------------------+
int CMql4TradeOps::_GetOrders(int order_type = ALL_ORDER_TYPES,string symbol = NULL)
{
   bool isNull = _FixSymbol(symbol);
   m_trades.Clear();
   int total = ::OrdersTotal();
   for(int i=0;i<total;i++)
   {
      if(::OrderSelect(i,SELECT_BY_POS))
      {
         if((isNull || ::OrderSymbol() == symbol)) 
         {
            if((m_magic == 0 || ::OrderMagicNumber() == m_magic))
            {
               if(
                     (order_type==ALL_ORDER_TYPES && !(::OrderType() == OP_BUY || ::OrderType() == OP_SELL))
                     ||
                     (order_type == ::OrderType())
                  )
               {
                  m_trades.Add(new COrder(::OrderTicket()));
               }
            }
          }
      }
      else
         return -1;
   }
   m_orderMode = WORKING_ORDERS;
   return m_trades.Total();
}
//+------------------------------------------------------------------+
int CMql4TradeOps::_Get_Pos_Orders(int order_type = ALL_ORDER_TYPES,string symbol = NULL)
{
   //_FixSymbol(symbol);
   CTradeVector temp;
   m_trades.FreeMode(false);
   temp.FreeMode(false);
   int total = _GetPositions(order_type, symbol);
   temp.AddArray(&m_trades);
   //for(int i=0;i<total;i++)
   //   temp.Add(this[i]);   
   
   total = _GetOrders(order_type, symbol);
   temp.AddArray(&m_trades);
   //for(int i=0;i<total;i++)
   //   temp.Add(this[i]); 
  
   //m_trades.Clear();
   m_trades.AssignArray(&temp);   
   m_trades.FreeMode(true); 
   m_orderMode = WORKING_POS_ORD;
   return m_trades.Total();
}
//+------------------------------------------------------------------+
int CMql4TradeOps::_Get_Pos_Deals(int order_type = ALL_ORDER_TYPES,string symbol = NULL)
{
   //_FixSymbol(symbol);
   CTradeVector temp;
   temp.FreeMode(false);
   m_trades.FreeMode(false);
   int total = _GetPositions(order_type, symbol);
   temp.AddArray(&m_trades);
   //for(int i=0;i<total;i++)
   //   temp.Add(this[i]);   
   total = _GetDeals(order_type, symbol);
   temp.AddArray(&m_trades);
   //for(int i=0;i<total;i++)
   //   temp.Add(this[i]); 
   //m_trades.Clear();
   m_trades.AssignArray(&temp);
   m_trades.FreeMode(true);
   m_orderMode = WORKING_POS_DEALS;
   return m_trades.Total();
}
//+------------------------------------------------------------------+
double CMql4TradeOps::_Point(string symbol = NULL)
{
   _FixSymbol(symbol);
   return ::SymbolInfoDouble(symbol,SYMBOL_POINT);
}
//+------------------------------------------------------------------+
int CMql4TradeOps::_Digits(string symbol = NULL)
{
   symbol = symbol == NULL ? ::Symbol() : symbol;
   return (int)::SymbolInfoInteger(symbol,SYMBOL_DIGITS);
}
 //+------------------------------------------------------------------+
double  CMql4TradeOps::_AdjPoint(string symbol = NULL)
{
   _FixSymbol(symbol);
   double point = ::SymbolInfoDouble(symbol,SYMBOL_POINT);
   if(!m_fixPips)
      return point;
   int    digits = (int)::SymbolInfoInteger(symbol,SYMBOL_DIGITS);
   if(digits == 5 || digits == 3)
   {
      return point * 10;
   }
   return point;
}
//+------------------------------------------------------------------+
int  CMql4TradeOps::_AdjDigits(string symbol = NULL)
{
   _FixSymbol(symbol);
   int    digits = (int)::SymbolInfoInteger(symbol,SYMBOL_DIGITS);
   if(!m_fixPips)
      return digits;
   if(digits == 5 || digits == 3)
   {
      return digits -1;
   }
   return digits;
}

//+------------------------------------------------------------------+
bool CMql4TradeOps::_FixSymbol(string &symbol)const
{
   bool res = false;
   if(symbol == NULL)
   {
      if(m_symbol == NULL)
      {
         symbol = ::Symbol();      
         res = true;
      }
      else
         symbol = m_symbol;
   }  
   return res;
}
//+------------------------------------------------------------------+
//|   Money Management Methods                                       |
//+------------------------------------------------------------------+

double CMql4TradeOps::_Lots(string symbol=NULL,double stop_loss=0.000000)
{
   _FixSymbol(symbol);
   if(m_money == MONEY_RISK)
   {
      return 0.0;
   }
   else if(m_money == MONEY_FIXED)
   {
      if(m_fixed_lots >= ::SymbolInfoDouble(symbol,SYMBOL_VOLUME_MIN) && m_fixed_lots <= ::SymbolInfoDouble(symbol,SYMBOL_VOLUME_MAX))
         return m_fixed_lots;
      else
      {
         ::Print(__FUNCTION__," OrderVolumeError: Invalid lot size.");
         return 0.0;
      }
   }
   else
   {
      return ::SymbolInfoDouble(symbol,SYMBOL_VOLUME_MIN);
   }
}