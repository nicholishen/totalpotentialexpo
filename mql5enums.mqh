//+------------------------------------------------------------------+
//|                                                    mql5enums.mqh |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property strict

enum  ENUM_SYMBOL_CALC_MODE
{
   SYMBOL_CALC_MODE_FOREX              = 0,
   SYMBOL_CALC_MODE_CFD                = 1,
   SYMBOL_CALC_MODE_FUTURES            = 2
};

enum ENUM_SYMBOL_SWAP_MODE
{
   SYMBOL_SWAP_MODE_POINTS             = 0,
   SYMBOL_SWAP_MODE_CURRENCY_SYMBOL    = 1,
   SYMBOL_SWAP_MODE_INTEREST_CURRENT   = 2,
   SYMBOL_SWAP_MODE_CURRENCY_MARGIN    = 3
};

/*
enum ENUM_ORDER_PROPERTY_INTEGER
{
   ORDER_TICKET

};
*/
ulong OrderGetTicket(const int index)
{
   if(OrderSelect(index,SELECT_BY_POS))
      return OrderTicket();
   else
      return 0;
}
/*

bool  HistoryOrderGetInteger( 
                              ulong ticket_number,     // Ticket 
                              ENUM_ORDER_PROPERTY_INTEGER  property_id,       // Property identifier 
                              long& long_var           // Here we accept the property value 
                              )
{
   long res = HistoryOrderGetInteger(ticket_number,property_id);
   if(res!=NULL)
   {
      long_var = res;
      return true;
   }
   else
      return false;
}
   
   
long  HistoryOrderGetInteger( 
   ulong                        ticket_number,     // Ticket 
   ENUM_ORDER_PROPERTY_INTEGER  property_id        // Property identifier 
   )
{
   if(OrderSelect((int)ticket_number,SELECT_BY_TICKET,MODE_HISTORY))
   {
      if(property_id == ORDER_TICKET)
         return OrderTicket();
      
   }
   return NULL;
}
*/
