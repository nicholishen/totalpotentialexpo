//+------------------------------------------------------------------+
//|                                            TotalExposureRisk.mq4 |
//|                                                      nicholishen |
//|                                   www.reddit.com/u/nicholishenFX |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property version   "1.00"
#property strict

#include "Mql4TradeOps.mqh"


CMql4TradeOps ops;
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
{
//---
   ops.Init();
   Risk();
   
}
//+------------------------------------------------------------------+

void Risk()
{
   int total = ops.TotalPositionsAndOrders();
   double total_loss=0;
   int  zero_stops = 0;
   for(int i=0;i<total;i++)
   {
      if(ops[i].OrderStopLoss() == 0)
      {
         zero_stops++;
         continue;
      }
      total_loss+= ops[i].MoneyAtRisk();
   } 
   double equity = AccountEquity();
   double risk = total_loss / equity;
   risk = NormalizeDouble(risk*100,2);
   string res;
   res+= "\nRisk assesment based on a total loss of "+IntegerToString(total)+" orders";
   res+= "\nTotal $ at risk = "+DoubleToString(total_loss,2);
   res = zero_stops > 0 ? res+"\nWARNING: you have "+IntegerToString(zero_stops)+" orders without stops. Cannot compute risk.":res;
   res+= "\nTotal Account Equity = "+DoubleToString(equity,2);
   res+= "\nTotal risk = "+DoubleToString(risk,2)+"%";
   Comment(res);
   Print(res);
   //Alert("Risk data commented on chart");
}
